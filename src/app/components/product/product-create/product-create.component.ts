import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { Router } from '@angular/router'
import { Product } from '../product.model';
@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css']
})
export class ProductCreateComponent implements OnInit {
  product: Product = {
    name: "",
    price:null
  }
  constructor(private productService: ProductService, private router: Router) {

   }

  ngOnInit(): void {

  }
  showEvent() {
    let { name, price } = this.product
    console.log(name,price);

  }
  createProduct() {
    if (this.product.name !== '' && this.product.price !== 0) {
      this.productService.create(this.product).subscribe(() => {

        this.productService.showMessage('Criado com sucesso!')
        this.router.navigate(['/products'])
      })
    }
     else if (this.product.name === '' || this.product.price === 0) {
        this.productService.showMessage('Preencha os dados!')

    }
  }
  cancel() {
    console.log('cancelando');
    this.router.navigate(['/products'])
  }
}

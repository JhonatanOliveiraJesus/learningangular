import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router'
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  // router : Router é equivalente a new Router()
  // Ou seja ele injeta automaticamente
  constructor(private router: Router) {

   }

  ngOnInit(): void {
  }
  navigateToProductCreate(): void{
    console.log('Navegando....');
    this.router.navigate(['/products/createProduct'])
  }
}

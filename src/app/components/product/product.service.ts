import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar'
import {HttpClient} from '@angular/common/http'
import { Product } from './product.model';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  baseUrl = 'http://localhost:3001/products'
  constructor(private snackBar: MatSnackBar, private httpClient: HttpClient) { }
  showMessage(msg:string):void {
    console.log(msg);
    this.snackBar.open(msg, 'X', {
      duration: 3000,
      horizontalPosition: "right",
      verticalPosition:"top"
    })
  }
  create(product: Product) : Observable<Product> {
   return this.httpClient.post<Product>(this.baseUrl, product)
  }
  getData(): Observable<Product[]>{
    return this.httpClient.get<Product[]>(this.baseUrl)
  }
  readById(id: string): Observable<Product> {
    const url = `${this.baseUrl}/${id}`
    return this.httpClient.get<Product>(url)
  }
  update(product: Product): Observable<Product>{
    const url = `${this.baseUrl}/${product.id}`
    return this.httpClient.put<Product>(url, product)
  }
  deleteProduct(id: string) : Observable<Product>{
    const url = `${this.baseUrl}/${id}`
    return this.httpClient.delete<Product>(url)
  }
}

import { ProductService } from './../product.service';
import { Component, OnInit } from '@angular/core';
import { Product } from '../product.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-read',
  templateUrl: './product-read.component.html',
  styleUrls: ['./product-read.component.css']
})
export class ProductReadComponent implements OnInit {
  products: Product[]
  constructor(private productService: ProductService,private router: Router)  { }
  displayedColumns = ['id', 'name','price', 'actions']
  ngOnInit(): void {
    this.productService.getData().subscribe(products => {
      this.products = products

    })
  }
  delete() {
    // this.productService.deleteProduct().subscribe
  }
}

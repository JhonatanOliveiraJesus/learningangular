import { ProductComponent } from './components/product/product.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './views/home/home.component';
import { ProductCreateComponent } from './components/product/product-create/product-create.component';
import { ProductUpdateComponent } from './components/product/product-update/product-update.component';


const routes: Routes = [{
  path:'',
  component:HomeComponent
},
  {
    path: 'products',
    component:ProductComponent

  },
  {
    path: 'products/createProduct',
    component:ProductCreateComponent
  },
  {
    path: 'products/updateProduct/:id',
    component: ProductUpdateComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

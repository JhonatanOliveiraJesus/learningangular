import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appAddPink]'
})
export class AddPinkDirective {

  constructor(private el: ElementRef) {
    el.nativeElement.style.color = '#ecec'
   }

}
